package com.tech4planet.aavc;

import android.app.Activity;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;
import java.util.HashMap;
// dummy change
public class HomeActivity extends AppCompatActivity implements NewLayout_Home.OnGet,NewLayout_Home.newGallery , NavigationView.OnNavigationItemSelectedListener,DrawerLayout.DrawerListener{
    private SliderLayout mDemoSlider;
    LinearLayout linearLayout;
    boolean exitactivity=true;
    public com.tech4planet.aavc.recyclerView.touchlistener t_listener;
    FrameLayout content_frame;
    boolean isDrawerOpen=false;
    ArrayList<View> views=null;
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    NavigationView navigationView;
    Fragment newfragment = null;
    int fragmentid=-1;
    LinearLayout poweredby;
   static final int fragmentno_newLayouthome=1,fragmentno_newLayoutmatch=2,fragmentno_newLayoutteams=3,fragmentno_newLayoutaboutus=4,fragmentno_newLayoutsocialmedia=5,fragmentno_newLayoutvenue=6,fragmentno_newLayoutLiveStream=7;
    static final int fragmentno_page4=7,fragmentno_gallery=8;
    Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        poweredby=(LinearLayout) findViewById(R.id.poweredby);
        poweredby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(HomeActivity.this,Weblayout.class);
                i.putExtra("msg","http://www.tech4planet.com/");
                startActivity(i);
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.addDrawerListener(this);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

//        View toRet = LayoutInflater.from(view.getContext()).inflate(R.layout.drawer_footer, listView, false);

        // Manipulate the view (if you need to) before calling addFooterView.


        navigationView.setNavigationItemSelectedListener(this);
         menu = navigationView.getMenu();

        setnavigationitemcolor(navigationView.getMenu());



content_frame=(FrameLayout)findViewById(R.id.content_frame);

//        content_frame.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                Toast.makeText(HomeActivity.this,"clicked",Toast.LENGTH_SHORT).show();
//                return true;
//            }
//        });
        newfragment=new NewLayout_Home();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);

        ft.commit();
    }


    /*@Override
    public boolean dispatchTouchEvent(MotionEvent event) {
//        t_listener.intercept_touch_in_fragment(event);
        View v = getCurrentFocus();
        try{

        }
        catch (Exception e)
        {

        }
        if (v instanceof FrameLayout) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x1 = event.getX();
                    break;
                case MotionEvent.ACTION_UP:
                    x2 = event.getX();
                    float deltaX = x2 - x1;

                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        switch (currentfragmentno)

                        {
                            case fragmentno_newLayouthome:
                                if (x1 > x2) {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                                    gotopage4();
                                } else if (x2 > x1) {
                                    //onBackPressed();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }
                                break;
                            case fragmentno_newLayoutmatch:
                                if (x2 > x1) {
                                    //gotonewlayouthome();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }

                                break;

                            case fragmentno_newLayoutteams:
                                if (x2 > x1) {
//                                gotonewlayouthome();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }
                                break;
                            case fragmentno_newLayoutaboutus:
                                if (x2 > x1) {
//                                gotonewlayouthome();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }
                                break;
                            case fragmentno_newLayoutsocialmedia:
                                if (x2 > x1) {
//                                gotonewlayouthome();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }
                                break;
                            case fragmentno_newLayoutvenue:
                                if (x2 > x1) {
//                                gotonewlayouthome();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }
                                break;
                            case fragmentno_page4:
                                if (x2 > x1) {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
//                                gotonewlayouthome();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_newLayouthome;
                                }

                                break;

                            case fragmentno_gallery:
                                if (x2 > x1) {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
//                                gotopage4();
                                    onBackPressed();
                                    currentfragmentno = fragmentno_page4;
                                }

                                break;

                        }
                        // Left to Right swipe action
                    if (x2 > x1)
                    {
                        Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                    }

                    // Right to left swipe action
                    else
                    {
                        Toast.makeText(this, "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                    }

                    } else {
                        // consider as something else - a screen tap for example
                    }
                    break;
            }

        boolean ret = super.dispatchTouchEvent(event);
        return ret;
    }*/
    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Melbourne_reg.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
    public void setnavigationitemcolor(Menu menu)
    {MenuItem item;
        SpannableString s;

        item= menu.findItem(R.id.match_schedule);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);

        item= menu.findItem(R.id.match_schedule);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());

        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.team_profile);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.player_profile);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.about_aavc);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.social_media);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.venue_profile);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.partners);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.contest);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.ticket_info);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.live_stream);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);
        item= menu.findItem(R.id.gallery);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);

       /* item= menu.findItem(R.id.organizing_team);
        applyFontToMenuItem(item);
        s = new SpannableString(item.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#7C0255")), 0, s.length(), 0);
        item.setTitle(s);*/
    }
    private void gotomatch() {

        newfragment=new Match();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
       // currentfragmentno=fragmentno_newLayoutmatch;

        ft.commit();
    }

    private void gotonewlayouthome() {
        exitactivity=true;
        newfragment=new NewLayout_Home();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        //currentfragmentno=fragmentno_newLayouthome;
        ft.commit();
    }
    public void gototeam()
    {
        newfragment=new Teams();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        //currentfragmentno=fragmentno_newLayoutteams;

        ft.commit();
    }

    public void gotoaboutus()
    {
        newfragment=new AboutUs();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        //currentfragmentno=fragmentno_newLayoutaboutus;

        ft.commit();
    }

    public void gotosocialmedia()
    {
        newfragment=new SocialMedia();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        //  currentfragmentno=fragmentno_newLayoutsocialmedia;

        ft.commit();
    }


    public void gotovenue()
    {
        newfragment=new Venue();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        // currentfragmentno=fragmentno_newLayoutvenue;

        ft.commit();
    }

    public void gotogallery()
    {newfragment=new Gallery();
        exitactivity=true;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        //currentfragmentno=fragmentno_gallery;
        ft.commit();

    }

    public void gotolivestream()
    {
        newfragment=new LiveStream();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, newfragment);
        ft.addToBackStack(null);
        // currentfragmentno=fragmentno_newLayoutLiveStream;
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //mDemoSlider.stopAutoCycle();
    }

    public void setExitactivity()
    {
        exitactivity=true;
    }
   @Override
    public void onBackPressed() {
       DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
       if (drawer.isDrawerOpen(GravityCompat.START)) {
           drawer.closeDrawer(GravityCompat.START);
       }
//        if(exitactivity){
//            super.onBackPressed();
//            exitactivity=false;
//        }
//       else
//        {  gotonewlayouthome();
//        }
      /* if(currentfragmentno==fragmentno_newLayoutmatch||currentfragmentno==fragmentno_newLayoutteams||currentfragmentno==fragmentno_newLayoutaboutus||currentfragmentno==fragmentno_newLayoutsocialmedia||currentfragmentno==fragmentno_newLayoutvenue||currentfragmentno==fragmentno_gallery)
       {
           currentfragmentno=-1;
           super.onBackPressed();
       }*/
       else
       {
           super.onBackPressed();

       }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about_us) {
            Intent intent = new Intent(HomeActivity.this, ContentActivity.class);
            intent.putExtra("content_type", "about_us");
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

   /* public void gotopage4()
    {
        exitactivity=false;
        Page4 startActivity=new Page4();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, startActivity);
        ft.addToBackStack(null);
        currentfragmentno=fragmentno_page4;
        ft.commit();
    }*/


    @Override
    public void get(FrameLayout linearLayout) {

    }

    @Override
    public void frame(FrameLayout frameLayout) {
        if(frameLayout.getId()==R.id.match)
        {
            gotomatch();

        }
        else if(frameLayout.getId()==R.id.team)
        {
            gototeam();

        }
        else if(frameLayout.getId()==R.id.about)
        {
           gotoaboutus();

        }
        else if(frameLayout.getId()==R.id.social)
        {
gotosocialmedia();

        }
        else if(frameLayout.getId()==R.id.venue)
        {

gotovenue();
        }
        else if(frameLayout.getId()==R.id.livestream)
        {
            gotolivestream();
        }

    }

   /* @Override
    public void onput(LinearLayout newlinearLayout) {


        if(newlinearLayout.getId()==R.id.leftarrow)
        {
//            gotonewlayouthome();
            onBackPressed();
            currentfragmentno=fragmentno_newLayouthome;

        }

    }*/

    @Override
    public void gallery(FrameLayout frameLayout) {
      if(frameLayout.getId()==R.id.gallery)
      {
         gotogallery();
      }
    }



    private void displaySelectedScreen(int itemId) {

        //creating fragment object



        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_home:
                newfragment = new NewLayout_Home();
                //getSupportActionBar().setTitle("Govt Services");
                break;
            case R.id.match_schedule:
                newfragment = new Match();
               // getSupportActionBar().setTitle("Shopping");
                break;
            case R.id.team_profile:
                newfragment = new Teams();
              //  getSupportActionBar().setTitle("Wallets");
                break;
            case R.id.player_profile:
                Toast.makeText(this,"Coming Soon",Toast.LENGTH_SHORT).show();
               // newfragment = new NewLayout_Home();
                break;
            case R.id.about_aavc:
                newfragment = new AboutUs();
                //getSupportActionBar().setTitle("Travel");
                break;
            case R.id.social_media:
                newfragment = new SocialMedia();
                //getSupportActionBar().setTitle("Food");
                break;
            case R.id.venue_profile:
                newfragment = new Venue();
               // getSupportActionBar().setTitle("Entertainment");
                break;
            case R.id.partners:
                startActivity(new Intent(this,Partners.class));
                break;
            case R.id.contest:
                Toast.makeText(this,"Coming Soon",Toast.LENGTH_SHORT).show();
                break;
            case R.id.ticket_info:
                Toast.makeText(this,"Coming Soon",Toast.LENGTH_SHORT).show();
                break;
            case R.id.gallery:
                newfragment = new Gallery();
                getSupportActionBar().setTitle("Health");
                break;
            case R.id.live_stream:
                newfragment = new LiveStream();
              //  getSupportActionBar().setTitle("Social Media");
                break;





        }

        //replacing the fragment

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {


        displaySelectedScreen(item.getItemId());

        if(item.getItemId()==R.id.player_profile)
        {
            newfragment=null;
        }
        if(item.getItemId()==R.id.partners)
        {
            newfragment=null;
        }
        if(item.getItemId()==R.id.ticket_info)
        {
            newfragment=null;
        }
        if(item.getItemId()==R.id.contest)
        {
            newfragment=null;
        }



        if (newfragment != null) {
            FragmentManager fm=getSupportFragmentManager();
            fm.popBackStack();
            android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.content_frame, newfragment);
            ft.addToBackStack(null);
            ft.commit();
        }
        // Handle navigation view item clicks here.
        /*int id = item.getItemId();

        if(id==R.id.nav_home)
        {
          gotonewlayouthome();
        }

        if (id == R.id.match_schedule) {

            gotomatch();// Handle the camera action
        }
        if(id==R.id.team_profile)
        {
         gototeam();
        }
        if(id==R.id.about_aavc)
        {
            gotoaboutus();
        }
        if(id==R.id.social_media)
        {
            gotosocialmedia();
        }

        if(id==R.id.venue_profile)
        {
            gotovenue();
        }

        if(id==R.id.gallery)
        {
            gotogallery();
        }

        if(id==R.id.live_stream)
        {
            gotolivestream();
        }

        if(id==R.id.organizing_team)
        {
            Intent i=new Intent(this,Weblayout.class);
            i.putExtra("msg","http://www.aavcup.com/organisational_structure");
            startActivity(i);
            finish();
        }*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAnimation(){

//        for(int i=1;i<views.size();i++)
//        {
//            View view;
//            view = views.get(i);
//
//        }

        for(int i=1;i<views.size();i++)
        {     Animation animation = AnimationUtils.loadAnimation(HomeActivity.this, android.R.anim.slide_in_left);
            View view;
            animation.setDuration(i * 10 + 200);
             view = views.get(i);
            view.startAnimation(animation);
            view.setVisibility(View.VISIBLE);
//            view.animate()
//                    .translationX(-view.getWidth())
//                    .alpha(1.0f)
//                    .setDuration(300);

        }
    }


    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
//        if (slideOffset == 0
//                && getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_STANDARD) {
//            // drawer closed
//            getActionBar()
//                    .setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//            invalidateOptionsMenu();
//        } else if (slideOffset != 0
//                && getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {
//            // started opening
//            getActionBar()
//                    .setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//            invalidateOptionsMenu();
//        }
//        super.onDrawerSlide(drawerView, slideOffset);

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        isDrawerOpen=true;
        setAnimation();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        isDrawerOpen=false;

    }

    @Override
    public void onDrawerStateChanged(int newState) {
        if (newState == DrawerLayout.STATE_SETTLING) {
            if (!isDrawerOpen) {
                if(views==null){
                   views=navigationView.getTouchables();
                }

                for(int i=1;i<views.size();i++)
                {
//    Animation animation = AnimationUtils.loadAnimation(HomeActivity.this, android.R.anim.slide_in_left);
                    View view;
//                    animation.setDuration(i * 50 + 200);
                    view = views.get(i);
                    view.setVisibility(View.INVISIBLE);

                }
                // starts opening
//                getActionBar()

//                        .setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            } else {
                // closing drawer
//                getActionBar()
//                        .setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            }
            invalidateOptionsMenu();
        }

    }
}
