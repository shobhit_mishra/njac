package com.tech4planet.aavc;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.tech4planet.aavc.recyclerView.cutomlinearlayout;
import com.tech4planet.aavc.recyclerView.touchlistener;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tech4planet on 3/3/17.
 */

public class NewLayout_Home extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    private SliderLayout mDemoSlider;
    FrameLayout match, team, player, about, social, venue,gallery,livestream,organizingteam,ticket,partners,contest;
    ArrayList<String> urls;

    static final int fragmentno_newLayouthome=1,fragmentno_newLayoutmatch=2,fragmentno_newLayoutteams=3,fragmentno_newLayoutaboutus=4,fragmentno_newLayoutsocialmedia=5,fragmentno_newLayoutvenue=6;
    static final int fragmentno_page4=7,fragmentno_gallery=8;
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    OnGet onget;
    newGallery ng;
    com.tech4planet.aavc.recyclerView.cutomlinearlayout linearLayout;


    public  boolean saveArray()
    {
        SharedPreferences sp = getActivity().getSharedPreferences("app_settings",getActivity().getApplicationContext().MODE_PRIVATE);
        SharedPreferences.Editor mEdit1 = sp.edit();
    /* sKey is an array */
        mEdit1.putInt("Status_size", urls.size());

        for(int i=0;i<urls.size();i++)
        {
            mEdit1.remove("Status_" + i);
            mEdit1.putString("Status_" + i, urls.get(i));
        }

        return mEdit1.commit();
    }

    public  void loadArray()
    {
        SharedPreferences mSharedPreference1 =   getActivity().getSharedPreferences("app_settings",getActivity().getApplicationContext().MODE_PRIVATE);
        urls.clear();
        int size = mSharedPreference1.getInt("Status_size", 0);

        for(int i=0;i<size;i++)
        {
            urls.add(mSharedPreference1.getString("Status_" + i, null));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View v=inflater.inflate(R.layout.newlayout_home,container,false);
        mDemoSlider = (SliderLayout)v.findViewById(R.id.newlayout_sliderlayout);
        gallery=(FrameLayout)v.findViewById(R.id.gallery);
        player=(FrameLayout)v.findViewById(R.id.player);
        ticket=(FrameLayout)v.findViewById(R.id.ticket);
        partners=(FrameLayout)v.findViewById(R.id.partners);
        contest=(FrameLayout)v.findViewById(R.id.contest);
        player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Coming Soon",Toast.LENGTH_SHORT).show();
            }
        });
        ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Coming Soon",Toast.LENGTH_SHORT).show();
            }
        });

        partners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Partners.class));
            }
        });

        contest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Coming Soon",Toast.LENGTH_SHORT).show();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ng.gallery(gallery);
            }
        });
        HomeActivity activity= (HomeActivity) getActivity();
        linearLayout=(cutomlinearlayout) v.findViewById(R.id.ll);
        linearLayout.setIlistener(new cutomlinearlayout.interceptlistener() {
            @Override
            public void interceptouch(MotionEvent event) {

                /*HomeActivity activity= (HomeActivity) getActivity();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x2 - x1;

                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            switch (activity.currentfragmentno)

                            {
                                case fragmentno_newLayouthome:
                                    if (x1 > x2) {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                                        activity.gotopage4();
                                    } else if (x2 > x1) {
                                        //onBackPressed();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }
                                    break;
                                case fragmentno_newLayoutmatch:
                                    if (x2 > x1) {
                                        //gotonewlayouthome();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }

                                    break;

                                case fragmentno_newLayoutteams:
                                    if (x2 > x1) {
//                                gotonewlayouthome();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }
                                    break;
                                case fragmentno_newLayoutaboutus:
                                    if (x2 > x1) {
//                                gotonewlayouthome();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }
                                    break;
                                case fragmentno_newLayoutsocialmedia:
                                    if (x2 > x1) {
//                                gotonewlayouthome();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }
                                    break;
                                case fragmentno_newLayoutvenue:
                                    if (x2 > x1) {
//                                gotonewlayouthome();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }
                                    break;
                                case fragmentno_page4:
                                    if (x2 > x1) {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
//                                gotonewlayouthome();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_newLayouthome;
                                    }

                                    break;

                                case fragmentno_gallery:
                                    if (x2 > x1) {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
//                                gotopage4();
                                        activity.onBackPressed();
                                        activity.currentfragmentno = fragmentno_page4;
                                    }

                                    break;

                            }
                            // Left to Right swipe action
                    if (x2 > x1)
                    {
                       // Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                    }

                    // Right to left swipe action
                    else
                    {
                        //Toast.makeText(this, "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                    }

                        } else {
                            // consider as something else - a screen tap for example
                        }
                        break;
                }*/
            }
        });
        com.tech4planet.aavc.recyclerView.touchlistener t_listener=new touchlistener() {
            @Override
            public void intercept_touch_in_fragment(MotionEvent event) {
                linearLayout.onInterceptTouchEvent(event);
            }
        };
        activity.t_listener=t_listener;
        FirebaseDatabase database=FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("second_page");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mDemoSlider.removeAllSliders();

                for(DataSnapshot itemsnapshot: dataSnapshot.getChildren()){
                    String title = itemsnapshot.child("title").getValue(String.class);
                    String url = itemsnapshot.child("url").getValue(String.class);
                    DefaultSliderView baseSliderView = new DefaultSliderView(getActivity());
                    baseSliderView
//                            .description(title)
                            .image(url);
                    mDemoSlider.addSlider(baseSliderView);

                }




            }



            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        match = (FrameLayout) v.findViewById(R.id.match);

        team = (FrameLayout) v.findViewById(R.id.team);
        //player = (FrameLayout) v.findViewById(R.id.player);
        about = (FrameLayout) v.findViewById(R.id.about);
        social = (FrameLayout) v.findViewById(R.id.social);
        venue = (FrameLayout) v.findViewById(R.id.venue);
        livestream=(FrameLayout)v.findViewById(R.id.livestream);
        organizingteam=(FrameLayout)v.findViewById(R.id.organizingteam);
         Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(700);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);


        match.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onget.frame(match);

            }
        });
        team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onget.frame(team);

            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onget.frame(about);

            }
        });
        social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onget.frame(social);

            }
        });

        venue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onget.frame(venue);

            }
        });
        livestream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onget.frame(livestream);
            }
        });
        organizingteam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),Weblayout.class);
                i.putExtra("msg","http://www.aavcup.com/organisational_structure");
                startActivity(i);
                
            }
        });

       /* HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("image1",R.drawable.layouts3);
        file_maps.put("image2",R.drawable.venue_img2);*/
//        for(String name : file_maps.keySet()){
//            TextSliderView textSliderView = new TextSliderView(getActivity());
//            // initialize a SliderLayout
//            textSliderView
//                    //.description(name)
//                    .image(file_maps.get(name))
//                   // .setScaleType(BaseSliderView.ScaleType.Fit)
//                    .setOnSliderClickListener(this);
//
//            //add your extra information
//            textSliderView.bundle(new Bundle());
//            /*textSliderView.getBundle()
//                    .putString("extra",name);*/
//
//            mDemoSlider.addSlider(textSliderView);
//        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Fade);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        //mDemoSlider.startAutoCycle(0,8000,false);
//        mDemoSlider.setDuration(8000);
        mDemoSlider.addOnPageChangeListener(this);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try
        {
            onget=(OnGet)activity;
            ng=(newGallery)activity;
        }
        catch (Exception e){}
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            mDemoSlider.setCurrentPosition(0);
        }catch (Exception e){}
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public interface OnGet
    {
        public void get(FrameLayout linearLayout);

        public void frame(FrameLayout frameLayout);
    }
    public  interface  newGallery
    {
        public void gallery(FrameLayout frameLayout);
    }
}
