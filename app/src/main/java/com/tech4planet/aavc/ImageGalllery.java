package com.tech4planet.aavc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by tech4planet on 6/3/17.
 */

public class ImageGalllery extends Activity {
    ArrayList<String> imageList;
    RecyclerView recyclerView;
    public ImageView imageView;
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    int currentfragmentno=0;
    static final int imagegallery=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagelayout);
        recyclerView=(RecyclerView)findViewById(R.id.rv);
        final FirebaseDatabase database =FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("images");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                imageList=new ArrayList<String>();
                for(DataSnapshot itemsnapshot:dataSnapshot.getChildren())
                {
                    String url = itemsnapshot.child("url").getValue(String.class);
                    imageList.add(url);

                }
                recyclerView.setLayoutManager(new LinearLayoutManager(ImageGalllery.this));
                recyclerView.setAdapter(new CustomAdapter(ImageGalllery.this));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

   /* @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
      *//*  if (v instanceof FrameLayout) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }*//*
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;

                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    switch (currentfragmentno)

                    {


                        case imagegallery:
                            if (x2 > x1)
                            {
//                                Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                                gotopage4();
                            }

                            break;

                    }
                    // Left to Right swipe action
                    *//*if (x2 > x1)
                    {
                        Toast.makeText(this, "Left to Right swipe [Next]", Toast.LENGTH_SHORT).show ();
                    }

                    // Right to left swipe action
                    else
                    {
                        Toast.makeText(this, "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                    }
*//*
                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        boolean ret = super.dispatchTouchEvent(event);
        return ret;
    }
*/
    private void gotopage4() {
        onBackPressed();
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

Context ctx;

        View itemView;

        public CustomAdapter(Context ctx) {
        this.ctx=ctx;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView= LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.image_gallery_item, parent, false);

            return new MyViewHolder(itemView,ctx);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            String url = imageList.get(position);
            ImageView imageView=(ImageView)holder.itemView;
            Picasso.with(ImageGalllery.this)
                    .load(url)
//                    .resize(600,300)
                    .into(imageView);


//            holder.title.setText(movie.getTitle());
//            holder.genre.setText(movie.getGenre());
//            holder.year.setText(movie.getYear());
        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            final Context ctx;

            ArrayList<String> newimageList=new ArrayList<String>();

            public MyViewHolder(final View view, final Context ctx) {
                super(view);
                this.ctx=ctx;

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // int position=getAdapterPosition();

                        imageView=(ImageView) v;

                        boolean b=false;
                        try {


                            BitmapDrawable drawable = (BitmapDrawable)imageView.getDrawable();
                            Bitmap bitmap = drawable.getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();

                            Intent in1 = new Intent(ImageGalllery.this, FullImage.class);
                            in1.putExtra("image",byteArray);
                            startActivity(in1);


                            if(bitmap!=null)
                            {
                                MediaStore.Images.Media.insertImage(ImageGalllery.this.getContentResolver(), bitmap, "AAVC image" , "Sample Description");
                                b=true;}
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                            b=false;
                        }
                        if(b){
                            Toast.makeText(ImageGalllery.this,"Saved to Gallery",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

//                title = (TextView) view.findViewById(R.id.title);
//                genre = (TextView) view.findViewById(R.id.genre);
//                year = (TextView) view.findViewById(R.id.year);
            }


        }

    }



}
