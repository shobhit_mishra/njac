package com.tech4planet.aavc;

import android.app.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

/**
 * Created by tech4planet on 2/3/17.
 */

public class SocialMedia extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    FrameLayout facebook,twitter,instagram,googleplus,linkedin,youtube;
    private SliderLayout mDemoSlider;
  /*  ImageView backarrow;*/
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    public void goback()
    {
        getActivity().onBackPressed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.socialmedia,container,false);
//        View v1=v.findViewById(R.id.relative_social_media);
       // FrameLayout rl_social=(FrameLayout) v.findViewById(R.id.rl_social_media);

        mDemoSlider = (SliderLayout)v.findViewById(R.id.newlayout_sliderlayout);
        FirebaseDatabase database=FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("social_media");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot itemsnapshot: dataSnapshot.getChildren()){
                    String title = itemsnapshot.child("title").getValue(String.class);
                    String url = itemsnapshot.child("url").getValue(String.class);
                    DefaultSliderView baseSliderView = new DefaultSliderView(getActivity());
                    baseSliderView
//                            .description(title)
                            .image(url);
                    mDemoSlider.addSlider(baseSliderView);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //backarrow = (ImageView)v.findViewById(R.id.iv_back);
        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(700);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);
       /* backarrow.startAnimation(mAnimation);
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity activity= (HomeActivity) getActivity();
                activity.currentfragmentno=HomeActivity.fragmentno_newLayouthome;
                activity.onBackPressed();
            }
        });*/
        facebook=(FrameLayout)v.findViewById(R.id.facebook);
        twitter=(FrameLayout)v.findViewById(R.id.twitter);
        instagram=(FrameLayout)v.findViewById(R.id.instagram);
        googleplus=(FrameLayout)v.findViewById(R.id.googleplus);
        linkedin=(FrameLayout)v.findViewById(R.id.linkedin);
        youtube=(FrameLayout)v.findViewById(R.id.youtube);
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),Weblayout.class);
                i.putExtra("msg","https://www.youtube.com/channel/UCd2tcAYPXP6gmB17fsQY9Dw");
                startActivity(i);
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),Weblayout.class);
                i.putExtra("msg","https://www.instagram.com/aavcup/");
                startActivity(i);
            }
        });
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),Weblayout.class);
                i.putExtra("msg","https://www.facebook.com/aavcup/");
                startActivity(i);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),Weblayout.class);
                i.putExtra("msg","https://twitter.com/aavcup");
                startActivity(i);

            }
        });
        googleplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(),Weblayout.class);
                i.putExtra("msg","https://plus.google.com/110656356498774417041");
                startActivity(i);


            }
        });

        linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),Weblayout.class);
               i.putExtra("msg","https://www.linkedin.com/in/aavcup");
                //i.putExtra("msg","http://www.aavcup.com/organisational_structure");
                startActivity(i);



            }
        });
      /*  HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("image1",R.drawable.social_img);
        file_maps.put("image2",R.drawable.social_img1);
        for(String name : file_maps.keySet()){
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    //.description(name)
                    .image(file_maps.get(name))
                    // .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            *//*textSliderView.getBundle()
                    .putString("extra",name);*//*

            mDemoSlider.addSlider(textSliderView);
        }*/
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Fade);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3000);
        mDemoSlider.addOnPageChangeListener(this);
        return v;
    }




    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}


