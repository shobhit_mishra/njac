package com.tech4planet.aavc;

/**
 * Created by u on 3/6/2017.
 */

public interface FragmentCallBack {
    public void onFragmentClicked(int i);
}
