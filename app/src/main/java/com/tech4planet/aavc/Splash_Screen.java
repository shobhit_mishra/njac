package com.tech4planet.aavc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.firebase.database.FirebaseDatabase;

public class Splash_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences mSharedPreference =  getSharedPreferences("app_settings",getApplicationContext().MODE_PRIVATE);

            boolean first_run=mSharedPreference.getBoolean("first_run",false);
            if(!first_run)
            {
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                SharedPreferences.Editor editor =mSharedPreference.edit();
                editor.putBoolean("first_run",true);
                editor.commit();
            }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        VideoView videoHolder = new VideoView(this);
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/"
                + R.raw.intro_animation); //do not add any extension

        videoHolder.setVideoURI(video);
        setContentView(videoHolder);
        videoHolder.start();
        videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Intent intent = new Intent(Splash_Screen.this,Buttonclass.class);
                    startActivity(intent);
                    finish();
            }
        });
//        Thread timerThread = new Thread(){
//            public void run(){
//                try{
//                    sleep(3000);
//                }catch(InterruptedException e){
//                    e.printStackTrace();
//                }finally{
//
//                }
//            }
//        };
//        timerThread.start();
    }
}
