package com.tech4planet.aavc;

import android.app.Activity;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BackgroundToForegroundTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tech4planet.aavc.R;

import java.util.HashMap;

import static android.media.CamcorderProfile.get;

/**
 * Created by tech4planet on 2/3/17.
 */

public class Match extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    private SliderLayout mDemoSlider;
ImageView img,arrowdwn,arrowup;

    private int currentImage = 0;
    int[] images = { R.drawable.may27, R.drawable.may29, R.drawable.june3, R.drawable.june2, R.drawable.june9, R.drawable.june11 };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.match,container,false);

//        arrowdwn=(ImageView)v.findViewById(R.id.arrowdwn);
       // arrowup=(ImageView)v.findViewById(R.id.arrowup);
//        arrowdwn.setOnClickListener(iButtonChangeImageListener);
         // where myresource (without the extension) is the file
       // arrowup.setOnClickListener(gButtonChangeImageListener);

        mDemoSlider = (SliderLayout)v.findViewById(R.id.newlayout_sliderlayout);
        FirebaseDatabase database=FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("match_schedule");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot itemsnapshot: dataSnapshot.getChildren()){
                    String title = itemsnapshot.child("title").getValue(String.class);
                    String url = itemsnapshot.child("url").getValue(String.class);
                    DefaultSliderView baseSliderView = new DefaultSliderView(getActivity());
                    baseSliderView
//                            .description(title)
                            .image(url);
                    mDemoSlider.addSlider(baseSliderView);
                  //  mDemoSlider.stopAutoCycle();
                }
//                mDemoSlider.setCurrentPosition(0);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
     /*   HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("image1",R.drawable.news1);
        file_maps.put("image2",R.drawable.news2);
        for(String name : file_maps.keySet()){
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    //.description(name)
                    .image(file_maps.get(name))
                    // .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }*/
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Fade);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3000);
        mDemoSlider.addOnPageChangeListener(this);

        return v;
    }


    View.OnClickListener iButtonChangeImageListener = new OnClickListener() {

        public void onClick(View v) {
            //Increase Counter to move to next Image
            currentImage++;
            currentImage = currentImage % images.length;
            img.setImageResource(images[currentImage]);

        }
    };

    /*View.OnClickListener gButtonChangeImageListener = new OnClickListener() {

        public void onClick(View v) {
            //Increase Counter to move to next Image
            currentImage--;
            currentImage = currentImage % images.length;

            img.setImageResource(images[currentImage]);

        }
    };*/

    @Override
    public void onStart() {
        super.onStart();
        try{
//            mDemoSlider.setCurrentPosition(0);

        }catch (Exception e){

        }
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
//        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }
    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
