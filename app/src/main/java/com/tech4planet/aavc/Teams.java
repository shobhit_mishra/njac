package com.tech4planet.aavc;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

/**
 * Created by tech4planet on 2/3/17.
 */

public class Teams extends Fragment implements FragmentCallBack ,BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    private SliderLayout mDemoSlider;
    ImageView ukblue,chennaiblue,bengalblue,biharblue,marathablue,upblue,haryanablue,delhiblue;
    /*boolean mShowingBackDelhi=false;
    boolean mShowingBackChennai=false;
    boolean mShowingBackBengal=false;
    boolean mShowingBackMaratha=false;
    boolean mShowingBackUP=false;
    boolean mShowingBackHaryana=false;
    boolean mShowingBackUttrakhand=false;
    boolean mShowingBackBihar=false;*/
     public void opendelhiblue(View v)
     {

     }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       /* CardFrontFragmentDelhi.fragmentCallBack=this;
        CardBackFragmentDelhi.fragmentCallBack=this;
        CardFrontFragmentChennai.fragmentCallBack=this;
        CardBackFragmentChennai.fragmentCallBack=this;
        CardFrontFragmentHaryana.fragmentCallBack=this;
        CardBackFragmentHaryana.fragmentCallBack=this;
        CardFrontFragmentUP.fragmentCallBack=this;
        CardBackFragmentUP.fragmentCallBack=this;
        CardFrontFragmentUttrakhand.fragmentCallBack=this;
        CardBackFragmentUttrakhand.fragmentCallBack=this;
        CardFrontFragmentMaratha.fragmentCallBack=this;
        CardBackFragmentMaratha.fragmentCallBack=this;
        CardFrontFragmentBengal.fragmentCallBack=this;
        CardBackFragmentBengal.fragmentCallBack=this;
        CardFrontFragmentBihar.fragmentCallBack=this;
        CardBackFragmentBihar.fragmentCallBack=this;*/

        View v=inflater.inflate(R.layout.team,null);
        ukblue=(ImageView)v.findViewById(R.id.ukblue);
        chennaiblue=(ImageView)v.findViewById(R.id.chennaiblue);
        bengalblue=(ImageView)v.findViewById(R.id.bengalblue);
        biharblue=(ImageView)v.findViewById(R.id.biharblue);
        marathablue=(ImageView)v.findViewById(R.id.marathablue);
        upblue=(ImageView)v.findViewById(R.id.upblue);
        delhiblue=(ImageView)v.findViewById(R.id.delhiblue);
        haryanablue=(ImageView)v.findViewById(R.id.haryanablue);
        mDemoSlider = (SliderLayout)v.findViewById(R.id.newlayout_sliderlayout);
        ukblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","uk");
                startActivity(i);
            }
        });
        chennaiblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","chennai");
                startActivity(i);
            }
        });
        bengalblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","bengal");
                startActivity(i);
            }
        });
        biharblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","bihar");
                startActivity(i);
            }
        });
        marathablue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","maratha");
                startActivity(i);
            }
        });
        upblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","up");
                startActivity(i);
            }
        });
        delhiblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","delhi");
                startActivity(i);
            }
        });
        haryanablue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(),TeamsDetails.class);
                i.putExtra("msg","haryana");
                startActivity(i);
            }
        });

        FirebaseDatabase database=FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("team_profile");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot itemsnapshot: dataSnapshot.getChildren()){
                    String title = itemsnapshot.child("title").getValue(String.class);
                    String url = itemsnapshot.child("url").getValue(String.class);
                    DefaultSliderView baseSliderView = new DefaultSliderView(getActivity());
                    baseSliderView
//                            .description(title)
                            .image(url);
                    mDemoSlider.addSlider(baseSliderView);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
     /*   if (savedInstanceState == null) {

            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container1, new CardFrontFragmentDelhi())
                    .commit();

            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container2, new CardFrontFragmentChennai())
                    .commit();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container3, new CardFrontFragmentBihar())
                    .commit();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container4, new CardFrontFragmentUttrakhand())
                    .commit();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container5, new CardFrontFragmentHaryana())
                    .commit();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container6, new CardFrontFragmentUP())
                    .commit();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container7, new CardFrontFragmentMaratha())
                    .commit();
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container8, new CardFrontFragmentBengal())
                    .commit();
        }
*/
       /* HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("image2",R.drawable.news3);
        file_maps.put("image1",R.drawable.news4);
        for(String name : file_maps.keySet()){
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    //.description(name)
                    .image(file_maps.get(name))
                    // .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            *//*textSliderView.getBundle()
                    .putString("extra",name);*//*

            mDemoSlider.addSlider(textSliderView);
        }*/
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Fade);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3000);
        mDemoSlider.addOnPageChangeListener(this);


        return v;
    }

    @Override
    public void onFragmentClicked(int n) {
    Log.d("fragment","clicked");
       // flipCard(n);
    }


    /**
     * A fragment representing the front of the card.
     */
   /* public static class CardFrontFragmentDelhi extends Fragment {
        static FragmentCallBack fragmentCallBack;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v =inflater.inflate(R.layout.delhi_partners_layout_card_front, container, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentCallBack.onFragmentClicked(1);
                }
            });
            return v;
        }
    }

    *//**
     * A fragment representing the back of the card.
     *//*
    public static class CardBackFragmentDelhi extends Fragment {
        static  FragmentCallBack fragmentCallBack;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v =inflater.inflate(R.layout.delhi_partners_layout_card_back, container, false);
            LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentCallBack.onFragmentClicked(1);
                }
            });
            return v;
        }

        public interface fragmentcallback{
            public void fragmentclicked();
        }
    }

    public static class CardFrontFragmentChennai extends Fragment {
        static FragmentCallBack fragmentCallBack;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v =inflater.inflate(R.layout.chennai_queens_layout_card_front, container, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentCallBack.onFragmentClicked(2);
                }
            });
            return v;
        }
        public interface fragmentcallback{
            public void fragmentclicked();
        }
    }

    *//**
     * A fragment representing the back of the card.
     *//*
    public static class CardBackFragmentChennai extends Fragment {
        static  FragmentCallBack fragmentCallBack;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v =inflater.inflate(R.layout.chennai_queens_layout_card_back, container, false);
            LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentCallBack.onFragmentClicked(2);
                }
            });
            return v;
        }  public interface fragmentcallback{
            public void fragmentclicked();
        }
    }

        public static class CardFrontFragmentBihar extends Fragment {
            static FragmentCallBack fragmentCallBack;
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View v =inflater.inflate(R.layout.bihar_challengers_layout_card_front, container, false);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragmentCallBack.onFragmentClicked(3);
                    }
                });
                return v;
            }
            public interface fragmentcallback{
                public void fragmentclicked();
            }
        }

        *//**
         * A fragment representing the back of the card.
         *//*
        public static class CardBackFragmentBihar extends Fragment {
            static  FragmentCallBack fragmentCallBack;
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View v =inflater.inflate(R.layout.bihar_challengers_layout_card_back, container, false);
                LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
                linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragmentCallBack.onFragmentClicked(3);
                    }
                });
                return v;
            }
            public interface fragmentcallback{
                public void fragmentclicked();
            }
        }
            public static class CardFrontFragmentUttrakhand extends Fragment {
                static FragmentCallBack fragmentCallBack;
                @Override
                public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                         Bundle savedInstanceState) {
                    View v =inflater.inflate(R.layout.uttrakhand_smashers_layout_card_front, container, false);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fragmentCallBack.onFragmentClicked(4);
                        }
                    });
                    return v;
                }
                public interface fragmentcallback{
                    public void fragmentclicked();
                }
            }

            *//**
             * A fragment representing the back of the card.
             *//*
            public static class CardBackFragmentUttrakhand extends Fragment {
                static  FragmentCallBack fragmentCallBack;
                @Override
                public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                         Bundle savedInstanceState) {
                    View v =inflater.inflate(R.layout.uttrakhand_smashers_layout_card_back, container, false);
                    LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
                    linearLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fragmentCallBack.onFragmentClicked(4);
                        }
                    });
                    return v;
                }
                public interface fragmentcallback{
                    public void fragmentclicked();
                }
            }

                public static class CardFrontFragmentHaryana extends Fragment {
                    static FragmentCallBack fragmentCallBack;
                    @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                             Bundle savedInstanceState) {
                        View v =inflater.inflate(R.layout.haryana_strikers_layout_card_front, container, false);
                        v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                fragmentCallBack.onFragmentClicked(5);
                            }
                        });
                        return v;
                    }
                    public interface fragmentcallback{
                        public void fragmentclicked();
                    }
                }

                *//**
                 * A fragment representing the back of the card.
                 *//*
                public static class CardBackFragmentHaryana extends Fragment {
                    static  FragmentCallBack fragmentCallBack;
                    @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                             Bundle savedInstanceState) {
                        View v =inflater.inflate(R.layout.haryana_strikers_layout_card_back, container, false);
                        LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
                        linearLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                fragmentCallBack.onFragmentClicked(5);
                            }
                        });
                        return v;
                    }
                    public interface fragmentcallback{
                        public void fragmentclicked();
                    }
                }

                    public static class CardFrontFragmentUP extends Fragment {
                        static FragmentCallBack fragmentCallBack;
                        @Override
                        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                 Bundle savedInstanceState) {
                            View v =inflater.inflate(R.layout.up_thunders_layout_card_front, container, false);
                            v.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fragmentCallBack.onFragmentClicked(6);
                                }
                            });
                            return v;
                        }
                        public interface fragmentcallback{
                            public void fragmentclicked();
                        }
                    }

                    *//**
                     * A fragment representing the back of the card.
                     *//*
                    public static class CardBackFragmentUP extends Fragment {
                        static  FragmentCallBack fragmentCallBack;
                        @Override
                        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                 Bundle savedInstanceState) {
                            View v =inflater.inflate(R.layout.up_thunders_layout_card_back, container, false);
                            LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
                            linearLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fragmentCallBack.onFragmentClicked(6);
                                }
                            });
                            return v;
                        }
                        public interface fragmentcallback{
                            public void fragmentclicked();
                        }
                    }

                        public static class CardFrontFragmentMaratha extends Fragment {
                            static FragmentCallBack fragmentCallBack;
                            @Override
                            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                     Bundle savedInstanceState) {
                                View v =inflater.inflate(R.layout.maratha_warriors_layout_card_front, container, false);
                                v.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        fragmentCallBack.onFragmentClicked(7);
                                    }
                                });
                                return v;
                            }
                            public interface fragmentcallback{
                                public void fragmentclicked();
                            }
                        }

                        *//**
                         * A fragment representing the back of the card.
                         *//*
                        public static class CardBackFragmentMaratha extends Fragment {
                            static  FragmentCallBack fragmentCallBack;
                            @Override
                            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                     Bundle savedInstanceState) {
                                View v =inflater.inflate(R.layout.maratha_warriors_layout_card_back, container, false);
                                LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.linearlayout);
                                linearLayout.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        fragmentCallBack.onFragmentClicked(7);
                                    }
                                });
                                return v;
                            }
                            public interface fragmentcallback{
                                public void fragmentclicked();
                            }
                        }

                            public static class CardFrontFragmentBengal extends Fragment {
                                static FragmentCallBack fragmentCallBack;
                                @Override
                                public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                         Bundle savedInstanceState) {
                                    View v =inflater.inflate(R.layout.bengal_tigress_layout_card_front, container, false);
                                    v.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            fragmentCallBack.onFragmentClicked(8);
                                        }
                                    });
                                    return v;
                                }
                                public interface fragmentcallback{
                                    public void fragmentclicked();
                                }
                            }

                            *//**
                             * A fragment representing the back of the card.
                             *//*
                            public static class CardBackFragmentBengal extends Fragment {
                                static FragmentCallBack fragmentCallBack;

                                @Override
                                public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                                         Bundle savedInstanceState) {
                                    View v = inflater.inflate(R.layout.bengal_tigress_layout_card_back, container, false);
                                    LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.linearlayout);
                                    linearLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            fragmentCallBack.onFragmentClicked(8);
                                        }
                                    });
                                    return v;
                                }
                                public interface fragmentcallback{
                                    public void fragmentclicked();
                                }
                            }


        private void flipCard(int n) {


            switch (n)
            {
                case 1: if (mShowingBackDelhi) {
                    mShowingBackDelhi=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container1, new CardFrontFragmentDelhi(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackDelhi = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container1, new CardBackFragmentDelhi())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
                case 2: if (mShowingBackChennai) {
                    mShowingBackChennai=false;
//                    manager2.popBackStack();
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container2, new CardFrontFragmentChennai())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackChennai = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container2, new CardBackFragmentChennai())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
                case 3: if (mShowingBackBihar) {
                    mShowingBackBihar=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container3, new CardFrontFragmentBihar(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackBihar = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container3, new CardBackFragmentBihar())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;

                case 4: if (mShowingBackUttrakhand) {
                    mShowingBackUttrakhand=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container4, new CardFrontFragmentUttrakhand(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackUttrakhand = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container4, new CardBackFragmentUttrakhand())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
                case 5: if (mShowingBackHaryana) {
                    mShowingBackHaryana=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container5, new CardFrontFragmentHaryana(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackHaryana = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                   getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container5, new CardBackFragmentHaryana())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
                case 6: if (mShowingBackUP) {
                    mShowingBackUP=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container6, new CardFrontFragmentUP(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackUP = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container6, new CardBackFragmentUP())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
                case 7: if (mShowingBackMaratha) {
                    mShowingBackMaratha=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container7, new CardFrontFragmentMaratha(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackMaratha = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container7, new CardBackFragmentMaratha())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
                case 8: if (mShowingBackBengal) {
                    mShowingBackBengal=false;
                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container8, new CardFrontFragmentBengal(),"")

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    return;
                }

                    // Flip to the back.

                    mShowingBackBengal = true;

                    // Create and commit a new fragment transaction that adds the fragment for
                    // the back of the card, uses custom animations, and is part of the fragment
                    // manager's back stack.

                    getFragmentManager()
                            .beginTransaction()

                            // Replace the default fragment animations with animator resources
                            // representing rotations when switching to the back of the card, as
                            // well as animator resources representing rotations when flipping
                            // back to the front (e.g. when the system Back button is pressed).
                            .setCustomAnimations(
                                    R.animator.card_flip_right_in,
                                    R.animator.card_flip_right_out,
                                    R.animator.card_flip_left_in,
                                    R.animator.card_flip_left_out)

                            // Replace any fragments currently in the container view with a
                            // fragment representing the next page (indicated by the
                            // just-incremented currentPage variable).
                            .replace(R.id.container8, new CardBackFragmentBengal())

                            // Add this transaction to the back stack, allowing users to press
                            // Back to get to the front of the card.
//                            .addToBackStack(null)

                            // Commit the transaction.
                            .commit();
                    break;
            }
        }*/
    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }



}



