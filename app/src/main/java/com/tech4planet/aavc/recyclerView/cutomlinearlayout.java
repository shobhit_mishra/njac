package com.tech4planet.aavc.recyclerView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * Created by u on 3/30/2017.
 */

public class cutomlinearlayout extends LinearLayout {
    interceptlistener ilistener;
    public cutomlinearlayout(Context context) {
        super(context);
    }

    public cutomlinearlayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        ilistener.interceptouch(ev);
        return false;
    }

    public void setIlistener(interceptlistener il)
    {
        ilistener=il;
    }
    public interface interceptlistener{
        public void interceptouch(MotionEvent ev);
    }
}
