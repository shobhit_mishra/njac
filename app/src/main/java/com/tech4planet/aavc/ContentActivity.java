package com.tech4planet.aavc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String ct=getIntent().getStringExtra("content_type");
        switch (ct)
        {
            case "about_us":   setContentView(R.layout.layout_about_us);
                break;
        }
        super.onCreate(savedInstanceState);

    }
}
