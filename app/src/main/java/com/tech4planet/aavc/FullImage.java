package com.tech4planet.aavc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

/**
 * Created by tech4planet on 8/3/17.
 */

public class FullImage extends Activity {
    ImageView fullimage;
    ScaleGestureDetector sgd;
    Matrix matrix;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullimage);
        fullimage=(ImageView)findViewById(R.id.fullimage);
       /* sgd=new ScaleGestureDetector(this,new ScaleListener());*/

        byte[] byteArray = getIntent().getByteArrayExtra("image");
        if(byteArray!=null) {
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            fullimage.setImageBitmap(bmp);
            matrix=new Matrix();
        }

    }
   /* @Override
    public boolean onTouchEvent(MotionEvent event) {
        sgd.onTouchEvent(event);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener{


        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float SF=detector.getScaleFactor();
            SF=Math.max(0.1f,Math.min(SF,0.5f));
            matrix.setScale(SF,SF);
            fullimage.setImageMatrix(matrix);
            return true;
        }
    }*/
}
