package com.tech4planet.aavc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class Partners extends Activity {
    ArrayList<String> imageList;
    RecyclerView recyclerView;
    public ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partners);
        recyclerView=(RecyclerView)findViewById(R.id.rv);
        final FirebaseDatabase database =FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("partners");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                imageList=new ArrayList<String>();
                for(DataSnapshot itemsnapshot:dataSnapshot.getChildren())
                {
                    String url = itemsnapshot.child("url").getValue(String.class);
                    imageList.add(url);

                }
                recyclerView.setLayoutManager(new LinearLayoutManager(Partners.this));
                recyclerView.setAdapter(new Partners.CustomAdapter(Partners.this));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public class CustomAdapter extends RecyclerView.Adapter<Partners.CustomAdapter.MyViewHolder> {

        Context ctx;

        View itemView;

        public CustomAdapter(Context ctx) {
            this.ctx=ctx;

        }

        @Override
        public Partners.CustomAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView= LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.image_gallery_item, parent, false);

            return new Partners.CustomAdapter.MyViewHolder(itemView,ctx);
        }



        @Override
        public void onBindViewHolder(Partners.CustomAdapter.MyViewHolder holder, int position) {
            String url = imageList.get(position);
            ImageView imageView=(ImageView)holder.itemView;
            Picasso.with(Partners.this)
                    .load(url)
//                    .resize(600,300)
                    .into(imageView);


//            holder.title.setText(movie.getTitle());
//            holder.genre.setText(movie.getGenre());
//            holder.year.setText(movie.getYear());
        }

        @Override
        public int getItemCount() {
            return imageList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            final Context ctx;

            ArrayList<String> newimageList=new ArrayList<String>();

            public MyViewHolder(final View view, final Context ctx) {
                super(view);
                this.ctx=ctx;

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // int position=getAdapterPosition();

                        imageView=(ImageView) v;

                        boolean b=false;
                        try {


                            BitmapDrawable drawable = (BitmapDrawable)imageView.getDrawable();
                            Bitmap bitmap = drawable.getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();

                            Intent in1 = new Intent(Partners.this, FullImage.class);
                            in1.putExtra("image",byteArray);
                            startActivity(in1);


                            if(bitmap!=null)
                            {
                                MediaStore.Images.Media.insertImage(Partners.this.getContentResolver(), bitmap, "AAVC image" , "Sample Description");
                                b=true;}
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                            b=false;
                        }
                        if(b){
                            Toast.makeText(Partners.this,"Saved to Gallery",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

//                title = (TextView) view.findViewById(R.id.title);
//                genre = (TextView) view.findViewById(R.id.genre);
//                year = (TextView) view.findViewById(R.id.year);
            }


        }

    }
}
