package com.tech4planet.aavc;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by tech4planet on 1/3/17.
 */

public class Buttonclass extends Activity {
    Button button;
    boolean result;
    private  SliderLayout sliderShow;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE= 123;
    Context context;

    protected void onCreate(Bundle savedInstancestate)

    {
        super.onCreate(savedInstancestate);
        setContentView(R.layout.buttonfile);

      /*  WebView gifImageView = (WebView) findViewById(R.id.GifImageView);
       gifImageView.loadUrl("file:///android_asset/aavcspining.html");*/
        sliderShow = (SliderLayout) findViewById(R.id.sliderLayout);
        FirebaseDatabase database=FirebaseDatabase.getInstance();
        DatabaseReference reference=database.getReference("news");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot itemsnapshot: dataSnapshot.getChildren()){
                    String title = itemsnapshot.child("title").getValue(String.class);
                    String url = itemsnapshot.child("url").getValue(String.class);
                    TextSliderView textSliderView = new TextSliderView(Buttonclass.this);
                    textSliderView
                            .description(title)
                            .image(url);
                    sliderShow.addSlider(textSliderView);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        button=(Button) findViewById(R.id.button);
        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(900);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);
        button.startAnimation(mAnimation);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.clearAnimation();
                result = checkPermission();

                if (result) {

                    gotointent();

                }



            }
        });
    }

   /* @Override
    public boolean onTouchEvent(MotionEvent event) {
        result = checkPermission();

        if (result) {

            gotointent();

        }
        return true;
        *//*return super.onTouchEvent(event);*//*

    }*/

    public void gotointent()
    {
        Intent intent=new Intent(Buttonclass.this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public boolean checkPermission()
    {
       /* int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
        {*/
            if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to save photos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(Buttonclass.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
       /* } else {
            return true;
        }*/
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   gotointent();
                } else {
//code for deny
                }
                break;
        }
    }

}
