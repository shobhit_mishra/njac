package com.tech4planet.aavc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.net.URISyntaxException;

import static android.R.id.message;

/**
 * Created by u on 4/7/2017.
 */

public class TeamsDetails extends Activity {
    String message;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        message =bundle.getString("msg");


        switch (message)
        {
            case "haryana":setContentView(R.layout.haryanastrikers);
                break;

            case "delhi":setContentView(R.layout.delhipanthers);
                break;

            case "up":setContentView(R.layout.upthunders);
                break;

            case "maratha":setContentView(R.layout.marathawarriors);
                break;

            case "bihar":setContentView(R.layout.biharchallengers);
                break;

            case "bengal":setContentView(R.layout.bengaltigress);
                break;

            case "chennai":setContentView(R.layout.chennaiqueens);
                break;

            case "uk":setContentView(R.layout.uttrakhandsmashers);
                break;

        }


    }
}
