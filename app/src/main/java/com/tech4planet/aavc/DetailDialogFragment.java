package com.tech4planet.aavc;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

/**
 * Created by tech4planet on 30/1/17.
 */

public class DetailDialogFragment extends DialogFragment {

    String detail_type="";

    public static DetailDialogFragment newInstance(String dt)
    {
        DetailDialogFragment detailDialogFragment=new DetailDialogFragment();
        detailDialogFragment.detail_type=dt;
        return  detailDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v=null;
        switch (detail_type)
        {
            case"contact_us":
                v=inflater.inflate(R.layout.layout_contact_us,null);
                Button b= (Button) v.findViewById(R.id.b_open_maps);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//
                        Uri gmmIntentUri = Uri.parse("geo:28.567493,77.176440?q=SABsoftzone");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);


                    }
                });
                break;
        }
        return v;
    }
}
